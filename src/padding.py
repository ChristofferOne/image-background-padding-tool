import numpy as np
from PIL import Image
import cv2
import time


def rasterize(image_data, bright_color_tolerance):
    for image_row in image_data:
        for image_pixel in image_row:
            if image_pixel[0] >= bright_color_tolerance \
                    and image_pixel[1] >= bright_color_tolerance \
                    and image_pixel[2] >= bright_color_tolerance:
                image_pixel[0] = 255
                image_pixel[1] = 255
                image_pixel[2] = 255
            else:
                image_pixel[0] = 0
                image_pixel[1] = 0
                image_pixel[2] = 0

    return image_data


def gcd(a, b):
    if b == 0:
        return a

    return gcd(a, a % b)


def aspect_ratio(a, b):
    if a == 0:
        return 0, 1

    if b == 0:
        return 1, 0

    r = gcd(b, a)

    return (a / r), (b / r)


def split_by_ratio(a, ratio):
    sum_ratio = sum(ratio)

    return (100 / sum_ratio) * ratio[0] * (a / 100), (100 / sum_ratio) * ratio[1] * (a / 100)


def calculate_image_padding(array, color, top_padding=0, bottom_padding=0, left_padding=0, right_padding=0):
    array_width = array.shape[1]
    color_size = color.shape[0]

    if top_padding > 0:
        pad = np.full((top_padding, array_width, color_size), color)

        array = np.concatenate((pad, array), axis=0)
    elif top_padding < 0:
        array = array[abs(top_padding):]

    if bottom_padding > 0:
        pad = np.full((bottom_padding, array_width, color_size), color)

        array = np.concatenate((array, pad), axis=0)
    elif bottom_padding < 0:
        array = array[:-abs(bottom_padding)]

    array_height = array.shape[0]
    color_size = color.shape[0]
    if left_padding > 0:
        pad = np.full((array_height, left_padding, color_size), color)

        array = np.concatenate((pad, array), axis=1)
    elif left_padding < 0:
        array = array[:, abs(left_padding):]

    if right_padding > 0:
        pad = np.full((array_height, right_padding, color_size), color)

        array = np.concatenate((array, pad), axis=1)
    elif right_padding < 0:
        array = array[:, :-abs(right_padding)]

    return array


def sample_corners(data, tolerance):
    data_shape = data.shape

    corners = np.array([
        data[0, 0],
        data[data_shape[0] - 1, 0],
        data[0, data_shape[1] - 1],
        data[data_shape[0] - 1, data_shape[1] - 1]
    ])

    if (sum(corners[0]) / 3) < tolerance:
        print("Color tolerance overstep.")
        return False

    corners.flatten()
    equal = np.array_equal(corners, np.roll(corners, 1))

    return equal


class Padding:
    BRIGHT_COLOR_TOLERANCE = 242
    HEIGHT_THRESHOLD = None
    WIDTH_THRESHOLD = None

    KEEP_ASPECT_RATIO = True
    KEEP_SIZE = True

    BASE_DIRECTORY = None
    OUTPUT_DIRECTORY = None

    AXIS = 0
    FILENAME = None

    PERCENTAGES = True
    DYNAMIC_ASPECT_RATIO = True

    rgb_image = None
    raster_image = None
    top_padding, bottom_padding, left_padding, right_padding = None, None, None, None
    relative_top_padding, relative_bottom_padding = None, None
    relative_left_padding, relative_right_padding = None, None
    image_original_size = None
    image_data = None

    def __init__(self, filename=None, axis=0, height=None, width=None, keep_aspect=True, keep_size=True, input_dir=None, output_dir=None):
        self.FILENAME = filename
        self.AXIS = axis
        self.HEIGHT_THRESHOLD = height
        self.WIDTH_THRESHOLD = width
        self.KEEP_ASPECT_RATIO = keep_aspect
        self.KEEP_SIZE = keep_size

        self.BASE_DIRECTORY = input_dir
        self.OUTPUT_DIRECTORY = output_dir

        self.DEBUG = False

    def set_input_directory(self, directory):
        self.BASE_DIRECTORY = directory

    def set_output_directory(self, directory):
        self.OUTPUT_DIRECTORY = directory

    def set_filename(self, filename):
        self.FILENAME = filename

    def load_image(self):
        image = Image.open(self.BASE_DIRECTORY + '/' + self.FILENAME)

        self.image_original_size = image.size

        image_data = Image.new("RGB", image.size)
        image_data.paste(image)

        self.rgb_image = np.array(image_data)
        self.image_data = np.array(image_data)

    def rasterize_image(self):
        image = cv2.imread(self.BASE_DIRECTORY + '/' + self.FILENAME)

        raster = cv2.threshold(image, self.BRIGHT_COLOR_TOLERANCE, 255, cv2.THRESH_BINARY)
        self.raster_image = cv2.cvtColor(raster[1], cv2.COLOR_BGR2GRAY)

        if self.DEBUG:
            cv2.imwrite(self.BASE_DIRECTORY + '/raster-' + self.FILENAME, self.raster_image)

    def calculate_padding(self):
        array = self.raster_image

        top_area = array[:round(np.size(array, 0) / 2)]
        bottom_area = array[round(np.size(array, 0) / 2):]
        left_area = np.array_split(array, 3, axis=1)[0]
        right_area = np.array_split(array, 3, axis=1)[-1]

        top_padding = top_area[np.all(top_area == 255, axis=1)]
        bot_padding = bottom_area[np.all(bottom_area == 255, axis=1)]
        left_padding = left_area[:, np.all(left_area == 255, axis=0)]
        right_padding = right_area[:, np.all(right_area == 255, axis=0)]

        self.top_padding = top_padding.shape[0]
        self.bottom_padding = bot_padding.shape[0]
        self.left_padding = left_padding.shape[1]
        self.right_padding = right_padding.shape[1]

    def calculate_percentages(self):
        # TODO: I'll just override this to get 10%, you solve it if you can be bothered.
        image_height = self.rgb_image.shape[0] - self.top_padding - self.bottom_padding
        image_width = self.rgb_image.shape[1] - self.left_padding - self.right_padding

        # decimal_height_percentage = (self.HEIGHT_THRESHOLD / 2 / 100)
        # decimal_width_percentage = (self.WIDTH_THRESHOLD / 2 / 100)

        # self.HEIGHT_THRESHOLD = image_height * decimal_height_percentage
        # self.WIDTH_THRESHOLD = image_width * decimal_width_percentage

        self.HEIGHT_THRESHOLD = round(image_height / 8)
        self.WIDTH_THRESHOLD = round(image_width / 8)

    def calculate_relative_padding(self):
        self.relative_top_padding = self.HEIGHT_THRESHOLD - self.top_padding
        self.relative_bottom_padding = self.HEIGHT_THRESHOLD - self.bottom_padding
        self.relative_left_padding = self.WIDTH_THRESHOLD - self.left_padding
        self.relative_right_padding = self.WIDTH_THRESHOLD - self.right_padding

    def calculate_x_axis_padding(self, image_aspect_ratio, width_padding, height_padding_aspect_ratio):
        height_padding = width_padding * image_aspect_ratio[0]

        self.relative_top_padding, self.relative_bottom_padding = split_by_ratio(
            height_padding,
            height_padding_aspect_ratio
        )

        if self.relative_top_padding < 0 < self.relative_left_padding:
            self.relative_top_padding = abs(self.relative_top_padding)
            self.relative_bottom_padding = abs(self.relative_bottom_padding)
        elif self.relative_top_padding > 0 > self.relative_left_padding:
            self.relative_top_padding = -self.relative_top_padding
            self.relative_bottom_padding = -self.relative_bottom_padding

        self.relative_top_padding = round(self.relative_top_padding)
        self.relative_bottom_padding = round(self.relative_bottom_padding)

    def calculate_y_axis_padding(self, image_aspect_ratio, height_padding, width_padding_aspect_ratio):
        width_padding = height_padding * 1 / image_aspect_ratio[0]

        self.relative_left_padding, self.relative_right_padding = split_by_ratio(
            width_padding,
            width_padding_aspect_ratio
        )

        if self.relative_left_padding < 0 < self.relative_top_padding:
            self.relative_left_padding = abs(self.relative_left_padding)
            self.relative_right_padding = abs(self.relative_right_padding)
        elif self.relative_left_padding > 0 > self.relative_top_padding:
            self.relative_left_padding = -self.relative_left_padding
            self.relative_right_padding = -self.relative_right_padding

        self.relative_left_padding = round(self.relative_left_padding)
        self.relative_right_padding = round(self.relative_right_padding)

    def account_for_aspect_ratio(self):
        if self.DYNAMIC_ASPECT_RATIO:
            self.dynamic_aspect_ratio()
        else:
            self.max_aspect_ratio()

    def max_aspect_ratio(self):
        image_height = self.rgb_image.shape[0]
        image_width = self.rgb_image.shape[1]

        image_aspect_ratio = aspect_ratio(image_height, image_width)

        horizontal_padding = self.left_padding + self.right_padding
        vertical_padding = self.top_padding + self.bottom_padding

        total_width_padding = sum((self.relative_left_padding, self.relative_right_padding))
        total_height_padding = sum((self.relative_top_padding, self.relative_bottom_padding))

        width_padding_aspect_ratio = aspect_ratio(abs(self.relative_left_padding), abs(self.relative_right_padding))
        height_padding_aspect_ratio = aspect_ratio(abs(self.relative_top_padding), abs(self.relative_bottom_padding))

        axis = 0
        # FIXME: Not really sure about this. Maybe don't multiply them with their aspect ratio
        if horizontal_padding * image_aspect_ratio[0] > vertical_padding * image_aspect_ratio[1]:
            axis = 1

        if axis == 1:
            self.calculate_y_axis_padding(
                image_aspect_ratio,
                total_height_padding,
                width_padding_aspect_ratio
            )
        else:
            self.calculate_x_axis_padding(
                image_aspect_ratio,
                total_width_padding,
                height_padding_aspect_ratio
            )

    def dynamic_aspect_ratio(self):
        image_height = self.rgb_image.shape[0]
        image_width = self.rgb_image.shape[1]

        # Padding Ratios
        image_aspect_ratio = aspect_ratio(image_height, image_width)
        width_padding_aspect_ratio = aspect_ratio(abs(self.relative_left_padding), abs(self.relative_right_padding))
        height_padding_aspect_ratio = aspect_ratio(abs(self.relative_top_padding), abs(self.relative_bottom_padding))

        # Padding summations
        total_width_padding = sum((self.relative_left_padding, self.relative_right_padding))
        total_height_padding = sum((self.relative_top_padding, self.relative_bottom_padding))

        # Prevent cropping padding
        width_padding = total_height_padding * 1 / image_aspect_ratio[0]
        predicted_left_padding, predicted_right_padding = split_by_ratio(width_padding, width_padding_aspect_ratio)

        height_padding = total_width_padding * image_aspect_ratio[0]
        predicted_top_padding, predicted_bottom_padding = split_by_ratio(height_padding, height_padding_aspect_ratio)

        c_left, c_right, c_top, c_bottom = self.calculate_critical_padding(
            predicted_left_padding,
            predicted_right_padding,
            predicted_top_padding,
            predicted_bottom_padding
        )

        if c_left < 0 or c_right < 0:
            print('Switching to Axis 1')

            self.AXIS = 1

        if c_top < 0 or c_bottom < 0:
            print('Switching to Axis 0')

            self.AXIS = 0

        if self.AXIS == 0:
            self.calculate_y_axis_padding(
                image_aspect_ratio,
                total_height_padding,
                width_padding_aspect_ratio
            )
        else:
            self.calculate_x_axis_padding(
                image_aspect_ratio,
                total_width_padding,
                height_padding_aspect_ratio
            )

    def center_compensation(self):
        if self.AXIS == 1:
            relative_bottom_padding = 0
            relative_top_padding = 0

            total_relative_height_padding = self.relative_top_padding + self.relative_bottom_padding

            padding_difference = self.top_padding - self.bottom_padding

            if padding_difference > 0:
                relative_bottom_padding += padding_difference
            else:
                relative_top_padding += abs(padding_difference)

            total_relative_height_padding = total_relative_height_padding - padding_difference

            half_relative_height_padding = round(total_relative_height_padding / 2)
            relative_top_padding += half_relative_height_padding
            relative_bottom_padding += half_relative_height_padding

            self.relative_top_padding = relative_top_padding
            self.relative_bottom_padding = relative_bottom_padding
        else:
            relative_right_padding = 0
            relative_left_padding = 0

            total_relative_width_padding = self.relative_left_padding + self.relative_right_padding

            padding_difference = self.left_padding - self.right_padding

            if padding_difference > 0:
                relative_right_padding += padding_difference
            else:
                relative_left_padding += abs(padding_difference)

            total_relative_width_padding = total_relative_width_padding - padding_difference

            half_relative_height_padding = round(total_relative_width_padding / 2)
            relative_left_padding += half_relative_height_padding
            relative_right_padding += half_relative_height_padding

            self.relative_left_padding = relative_left_padding
            self.relative_right_padding = relative_right_padding

    def calculate_critical_padding(self, left_padding, right_padding, top_padding, bottom_padding):
        left_critical = left_padding + self.left_padding
        right_critical = right_padding + self.right_padding
        top_critical = top_padding + self.top_padding
        bottom_critical = bottom_padding + self.bottom_padding

        return left_critical, right_critical, top_critical, bottom_critical

    def apply_padding(self):
        padding_color = self.rgb_image[0][0]
        self.rgb_image = calculate_image_padding(
            self.rgb_image,
            padding_color,
            round(self.relative_top_padding),
            round(self.relative_bottom_padding),
            round(self.relative_left_padding),
            round(self.relative_right_padding)
        )

    def resize_image(self, image):
        return image.resize(self.image_original_size, Image.ANTIALIAS)

    def save_image(self):
        image = Image.fromarray(self.rgb_image)

        if self.KEEP_SIZE:
            image = self.resize_image(image)

        image.save(self.OUTPUT_DIRECTORY + '/' + self.FILENAME)

    def set_image_padding(self, filename=None, height=None, width=None, keep_aspect=None, keep_size=None):
        start_time = time.time()
        if filename:
            self.FILENAME = filename

        if keep_aspect:
            self.KEEP_ASPECT_RATIO = keep_aspect

        if keep_size:
            self.KEEP_SIZE = keep_size

        if height:
            self.HEIGHT_THRESHOLD = height

        if width:
            self.WIDTH_THRESHOLD = width

        if self.BASE_DIRECTORY is None:
            raise Exception('Input directory has not been set.')

        if self.OUTPUT_DIRECTORY is None:
            raise Exception('Output directory has not been set.')

        if self.FILENAME is None:
            raise Exception('Filename has not been set')

        if self.HEIGHT_THRESHOLD is None:
            raise Exception('Height has not been set')

        if self.WIDTH_THRESHOLD is None:
            raise Exception('Width has not been set')

        print('Processing image ' + self.FILENAME)

        self.load_image()

        isolated = sample_corners(self.image_data, self.BRIGHT_COLOR_TOLERANCE)

        if not isolated:
            print('Unsupported Image. Aborting process.')
            return

        self.rasterize_image()
        self.calculate_padding()

        if self.PERCENTAGES:
            self.calculate_percentages()

        self.calculate_relative_padding()

        if self.KEEP_ASPECT_RATIO:
            self.account_for_aspect_ratio()
            self.center_compensation()

        self.apply_padding()

        self.save_image()
        print('Time: ', time.time() - start_time)
