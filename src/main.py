import numpy as np
from PIL import Image

# Define Constants
BRIGHT_COLOR_TOLERANCE = 230
# TODO: Maybe use * or something to keep padding.
HEIGHT_THRESHOLD = 160
WIDTH_THRESHOLD = 50
KEEP_ASPECT_RATIO = True
KEEP_SIZE = True

BASE_DIRECTORY = 'storage'
OUTPUT_DIRECTORY = 'storage/outputs'
FILE_NAME = ''


# Methods
def calculate_padding(array):
    array = np.apply_along_axis(np.maximum.reduce, 2, array)

    top_area = array[:round(np.size(array, 0) / 2)]
    bottom_area = array[round(np.size(array, 0) / 2):]
    left_area = np.array_split(array, 3, axis=1)[0]
    right_area = np.array_split(array, 3, axis=1)[-1]

    top_padding = top_area[np.all(top_area == 255, axis=1)]
    bot_padding = bottom_area[np.all(bottom_area == 255, axis=1)]
    left_padding = left_area[:, np.all(left_area == 255, axis=0)]
    right_padding = right_area[:, np.all(right_area == 255, axis=0)]

    return top_padding.shape[0], bot_padding.shape[0], left_padding.shape[1], right_padding.shape[1]


def measure_width_at(obj, index, inverse=False):
    process_pad = 0

    if inverse:
        process_row = reversed(obj[index])
    else:
        process_row = obj[index]

    for process_pixel in process_row:
        if process_pixel[0] == 255 and process_pixel[1] == 255 and process_pixel[2] == 255:
            process_pad += 1
        else:
            break

    return process_pad


def measure_height_at(obj, index, inverse=False):
    process_pad = 0

    if inverse:
        process_row = reversed(obj[:, [index]])
    else:
        process_row = obj[:, [index]]

    for process_pixel in process_row:
        if process_pixel[0][0] == 255 and process_pixel[0][1] == 255 and process_pixel[0][2] == 255:
            process_pad += 1
        else:
            break

    return process_pad


def sample_corners(data):
    data_shape = data.shape

    corners = np.array([
        data[0, 0],
        data[data_shape[0] - 1, 0],
        data[0, data_shape[1] - 1],
        data[data_shape[0] - 1, data_shape[1] - 1]
    ])

    if (sum(corners[0]) / 3) < BRIGHT_COLOR_TOLERANCE:
        print("Color tolerance overstep.")
        return False

    corners.flatten()
    equal = np.array_equal(corners, np.roll(corners, 1))

    return equal


def rasterize(image_data):
    for image_row in image_data:
        for image_pixel in image_row:
            if image_pixel[0] >= BRIGHT_COLOR_TOLERANCE \
                    and image_pixel[1] >= BRIGHT_COLOR_TOLERANCE \
                    and image_pixel[2] >= BRIGHT_COLOR_TOLERANCE:
                image_pixel[0] = 255
                image_pixel[1] = 255
                image_pixel[2] = 255
            else:
                image_pixel[0] = 0
                image_pixel[1] = 0
                image_pixel[2] = 0

    return image_data


def apply_padding(array, color, top_padding=0, bottom_padding=0, left_padding=0, right_padding=0):
    if top_padding > 0:
        for _ in range(top_padding):
            array = np.insert(array, 0, color, axis=0)
    else:
        array = array[abs(top_padding):]

    if bottom_padding > 0:
        for _ in range(bottom_padding):
            array = np.insert(array, -1, color, axis=0)
    else:
        array = array[:-abs(bottom_padding)]

    if left_padding > 0:
        for _ in range(left_padding):
            array = np.insert(array, 0, color, axis=1)
    else:
        array = array[:, abs(left_padding):]

    if right_padding > 0:
        for _ in range(right_padding):
            array = np.insert(array, -1, color, axis=1)
    else:
        array = array[:, :-abs(right_padding)]

    return array


def gcd(a, b):
    if b == 0:
        return a

    return gcd(a, a % b)


def aspect_ratio(a, b):
    r = gcd(b, a)

    return (a / r), (b / r)


def split_by_ratio(a, ratio):
    sum_ratio = sum(ratio)

    return (100 / sum_ratio) * ratio[0] * (a / 100), (100 / sum_ratio) * ratio[1] * (a / 100)


def runner():
    # Algorithm Runner
    img = Image.open(BASE_DIRECTORY + '/' + FILE_NAME)

    rgb_image = Image.new("RGB", img.size)
    rgb_image.paste(img)

    pixels = np.array(rgb_image)

    # Make sure all corners contains the same pixel color.
    if not sample_corners(pixels):
        print("Mismatch in corner values.")
        print("Make sure that the image has a uniform background spanning all corners.")
        exit()

    image_array = np.array(rgb_image)
    rasterized_image = rasterize(image_array)

    # Calculate Padding
    top_padding, bottom_padding, left_padding, right_padding = calculate_padding(rasterized_image)

    # If no padding is found image might not have any padding to work with.
    if sum([top_padding, bottom_padding, left_padding, right_padding]) < 50:
        print("Warning: The padding in this image could not be correctly identified!")
        print("Existing padding should be no less than 50 pixels total.")
        print("Make sure that the image has a uniform background spanning all corners.")
        exit()

    # Calculate Relative Padding
    relative_top_padding = HEIGHT_THRESHOLD - top_padding
    relative_bottom_padding = HEIGHT_THRESHOLD - bottom_padding
    relative_left_padding = WIDTH_THRESHOLD - left_padding
    relative_right_padding = WIDTH_THRESHOLD - right_padding

    if (relative_left_padding < 0 or relative_right_padding < 0) and KEEP_ASPECT_RATIO:
        print("Negative padding not yet supported!")

    # Keep Aspect Ratio
    # TODO: Make this a method and make it agnostic for input order.
    # TODO: Only works if width is larger than height.
    if KEEP_ASPECT_RATIO:
        image_height = pixels.shape[0]
        image_width = pixels.shape[1]

        # Ratios
        image_aspect_ratio = aspect_ratio(image_height, image_width)
        width_padding_aspect_ratio = aspect_ratio(relative_left_padding, relative_right_padding)
        height_padding_aspect_ratio = aspect_ratio(relative_top_padding, relative_bottom_padding)

        # Summation
        width_padding = sum((relative_left_padding, relative_right_padding))
        height_padding = sum((relative_top_padding, relative_bottom_padding))

        # Weights
        width_padding_weight = width_padding * image_aspect_ratio[1]
        height_padding_weight = height_padding * image_aspect_ratio[0]

        # width_padding = height_padding * 1 / image_aspect_ratio[0]

        suggested_left_padding, suggested_right_padding = split_by_ratio(width_padding, width_padding_aspect_ratio)
        left_critical = left_padding + suggested_left_padding
        right_critical = right_padding + suggested_right_padding

        if width_padding_weight > height_padding_weight or left_critical < 0 or right_critical < 0:
            print("Using width aspect")
            height_padding = width_padding * image_aspect_ratio[0]
            print(image_aspect_ratio)

            print(relative_top_padding, relative_bottom_padding)

            relative_top_padding, relative_bottom_padding = split_by_ratio(height_padding, height_padding_aspect_ratio)

            if relative_top_padding < 0 < relative_left_padding:
                relative_top_padding = abs(relative_top_padding)
                relative_bottom_padding = abs(relative_bottom_padding)
            elif relative_top_padding > 0 > relative_left_padding:
                relative_top_padding = -relative_top_padding
                relative_bottom_padding = -relative_bottom_padding
        else:
            print("Using height aspect")
            width_padding = height_padding * 1 / image_aspect_ratio[0]

            # relative_left_padding, relative_right_padding = suggested_left_padding, suggested_right_padding
            relative_left_padding, relative_right_padding = split_by_ratio(width_padding, width_padding_aspect_ratio)

            if relative_left_padding < 0 < relative_top_padding:
                relative_left_padding = abs(relative_left_padding)
                relative_right_padding = abs(relative_right_padding)
            elif relative_left_padding > 0 > relative_top_padding:
                relative_left_padding = -relative_left_padding
                relative_right_padding = -relative_right_padding

    print(round(relative_top_padding), round(relative_bottom_padding),
          round(relative_left_padding), round(relative_right_padding))

    # Apply Padding
    padding_pixel = pixels[0][0]
    pixels = apply_padding(
        pixels,
        padding_pixel,
        round(relative_top_padding),
        round(relative_bottom_padding),
        round(relative_left_padding),
        round(relative_right_padding)
    )

    # Save result image
    disp = Image.fromarray(pixels)

    if KEEP_SIZE:
        disp = disp.resize(img.size, Image.ANTIALIAS)

    disp.save(OUTPUT_DIRECTORY + '/' + FILE_NAME)


fns = ['2.jpg', '3.jpg', '4.jpg', '6.jpg', '7.jpg', '9.jpg', '10.jpg', 'test1.jpg', 'test2.jpg', 'test3.jpg']
# fns = ['9.jpg']

for name in fns:
    FILE_NAME = name
    runner()
