import numpy as np
from PIL import Image
import cv2


def rasterize(image_data, bright_color_tolerance):
    for image_row in image_data:
        for image_pixel in image_row:
            if image_pixel[0] >= bright_color_tolerance \
                    and image_pixel[1] >= bright_color_tolerance \
                    and image_pixel[2] >= bright_color_tolerance:
                image_pixel[0] = 255
                image_pixel[1] = 255
                image_pixel[2] = 255
            else:
                image_pixel[0] = 0
                image_pixel[1] = 0
                image_pixel[2] = 0

    return image_data


def gcd(a, b):
    if b == 0:
        return a

    return gcd(a, a % b)


def aspect_ratio(a, b):
    r = gcd(b, a)

    return (a / r), (b / r)


def split_by_ratio(a, ratio):
    sum_ratio = sum(ratio)

    return (100 / sum_ratio) * ratio[0] * (a / 100), (100 / sum_ratio) * ratio[1] * (a / 100)


def insert_image_padding(array, color, top_padding=0, bottom_padding=0, left_padding=0, right_padding=0):
    if top_padding > 0:
        row = np.full((top_padding, array.shape[1], 3), color[0])
        array = np.concatenate((row, array), axis=0)

    if bottom_padding > 0:
        row = np.full((bottom_padding, array.shape[1], 3), color[0])
        array = np.concatenate((array, row), axis=0)

    if left_padding > 0:
        col = np.array(np.full((array.shape[0], left_padding, 3), color[0]))
        array = np.concatenate((col, array), axis=1)

    if right_padding > 0:
        col = np.array(np.full((array.shape[0], right_padding, 3), color[0]))
        array = np.concatenate((array, col), axis=1)

    return array


def calculate_image_padding(array, color, top_padding=0, bottom_padding=0, left_padding=0, right_padding=0):
    array_width = array.shape[1]
    color_size = color.shape[0]

    if top_padding > 0:
        pad = np.full((top_padding, array_width, color_size), color)

        array = np.concatenate((pad, array), axis=0)
    elif top_padding < 0:
        array = array[abs(top_padding):]

    if bottom_padding > 0:
        pad = np.full((bottom_padding, array_width, color_size), color)

        array = np.concatenate((array, pad), axis=0)
    elif bottom_padding < 0:
        array = array[:-abs(bottom_padding)]

    array_height = array.shape[0]
    color_size = color.shape[0]
    if left_padding > 0:
        pad = np.full((array_height, left_padding, color_size), color)

        array = np.concatenate((pad, array), axis=1)
    elif left_padding < 0:
        array = array[:, abs(left_padding):]

    if right_padding > 0:
        pad = np.full((array_height, right_padding, color_size), color)

        array = np.concatenate((array, pad), axis=1)
    elif right_padding < 0:
        array = array[:, :-abs(right_padding)]

    return array


def sample_corners(data, tolerance):
    data_shape = data.shape

    corners = np.array([
        data[0, 0],
        data[data_shape[0] - 1, 0],
        data[0, data_shape[1] - 1],
        data[data_shape[0] - 1, data_shape[1] - 1]
    ])

    if (sum(corners[0]) / 3) < tolerance:
        print("Color tolerance overstep.")
        return False

    corners.flatten()
    equal = np.array_equal(corners, np.roll(corners, 1))

    return equal


class Square:
    BRIGHT_COLOR_TOLERANCE = 230
    HEIGHT_THRESHOLD = None
    WIDTH_THRESHOLD = None

    GRAY_SCALE = False

    KEEP_ASPECT_RATIO = True
    SQUARE = False
    KEEP_SIZE = True

    BASE_DIRECTORY = None
    OUTPUT_DIRECTORY = None

    AXIS = 0
    FILENAME = None

    rgb_image = None
    raster_image = None
    top_padding, bottom_padding, left_padding, right_padding = None, None, None, None
    relative_top_padding, relative_bottom_padding, relative_left_padding, relative_right_padding = None, None, None, None
    image_original_size = None
    image_data = None

    def __init__(self, filename=None, axis=0, height=None, width=None, keep_aspect=True, keep_size=True, input_dir=None, output_dir=None):
        self.FILENAME = filename
        self.AXIS = axis
        self.HEIGHT_THRESHOLD = height
        self.WIDTH_THRESHOLD = width
        self.KEEP_ASPECT_RATIO = keep_aspect,
        self.KEEP_SIZE = keep_size

        self.BASE_DIRECTORY = input_dir
        self.OUTPUT_DIRECTORY = output_dir

    def set_input_directory(self, dir):
        self.BASE_DIRECTORY = dir

    def set_output_directory(self, dir):
        self.OUTPUT_DIRECTORY = dir

    def set_filename(self, filename):
        self.FILENAME = filename

    def load_image(self):
        image = Image.open(self.BASE_DIRECTORY + '/' + self.FILENAME)

        if image.mode != 'RGB':
            print('Grayscale Warning')
            self.GRAY_SCALE = True

        self.image_original_size = image.size

        if self.GRAY_SCALE:
            image_data = Image.new("I", image.size).convert('RGB')
        else:
            image_data = Image.new("RGB", image.size)

        image_data.paste(image)

        self.rgb_image = np.array(image_data)
        self.image_data = np.array(image_data)

    def rasterize_image(self):
        image = cv2.imread(self.BASE_DIRECTORY + '/' + self.FILENAME)

        raster = cv2.threshold(image, self.BRIGHT_COLOR_TOLERANCE, 255, cv2.THRESH_BINARY)
        self.raster_image = cv2.cvtColor(raster[1], cv2.COLOR_BGR2GRAY)

    def rasterize_array(self, image):
        raster = cv2.threshold(image, self.BRIGHT_COLOR_TOLERANCE, 255, cv2.THRESH_BINARY)
        return cv2.cvtColor(raster[1], cv2.COLOR_BGR2GRAY)

    def calculate_padding(self, image, ret=False):
        array = image

        top_area = array[:round(np.size(array, 0) / 2)]
        bottom_area = array[round(np.size(array, 0) / 2):]
        left_area = np.array_split(array, 3, axis=1)[0]
        right_area = np.array_split(array, 3, axis=1)[-1]

        top_padding = top_area[np.all(top_area == 255, axis=1)]
        bot_padding = bottom_area[np.all(bottom_area == 255, axis=1)]
        left_padding = left_area[:, np.all(left_area == 255, axis=0)]
        right_padding = right_area[:, np.all(right_area == 255, axis=0)]

        if ret:
            return top_padding.shape[0], bot_padding.shape[0], left_padding.shape[1], right_padding.shape[1]

        self.top_padding = top_padding.shape[0]
        self.bottom_padding = bot_padding.shape[0]
        self.left_padding = left_padding.shape[1]
        self.right_padding = right_padding.shape[1]

    def calculate_relative_padding(self):
        self.relative_top_padding = self.HEIGHT_THRESHOLD - self.top_padding
        self.relative_bottom_padding = self.HEIGHT_THRESHOLD - self.bottom_padding
        self.relative_left_padding = self.WIDTH_THRESHOLD - self.left_padding
        self.relative_right_padding = self.WIDTH_THRESHOLD - self.right_padding

    def calculate_x_axis_padding(self, image_aspect_ratio, width_padding, height_padding_aspect_ratio):
        height_padding = width_padding * image_aspect_ratio[0]

        self.relative_top_padding, self.relative_bottom_padding = split_by_ratio(height_padding, height_padding_aspect_ratio)

        if self.relative_top_padding < 0 < self.relative_left_padding:
            self.relative_top_padding = abs(self.relative_top_padding)
            self.relative_bottom_padding = abs(self.relative_bottom_padding)
        elif self.relative_top_padding > 0 > self.relative_left_padding:
            self.relative_top_padding = -self.relative_top_padding
            self.relative_bottom_padding = -self.relative_bottom_padding

    def calculate_y_axis_padding(self, image_aspect_ratio, height_padding, width_padding_aspect_ratio):
        width_padding = height_padding * 1 / image_aspect_ratio[0]

        self.relative_left_padding, self.relative_right_padding = split_by_ratio(width_padding, width_padding_aspect_ratio)

        if self.relative_left_padding < 0 < self.relative_top_padding:
            self.relative_left_padding = abs(self.relative_left_padding)
            self.relative_right_padding = abs(self.relative_right_padding)
        elif self.relative_left_padding > 0 > self.relative_top_padding:
            self.relative_left_padding = -self.relative_left_padding
            self.relative_right_padding = -self.relative_right_padding

    def account_for_aspect_ratio(self):
        image_height = self.rgb_image.shape[0]
        image_width = self.rgb_image.shape[1]

        # Padding Ratios
        image_aspect_ratio = aspect_ratio(image_height, image_width)
        width_padding_aspect_ratio = aspect_ratio(self.relative_left_padding, self.relative_right_padding)
        height_padding_aspect_ratio = aspect_ratio(self.relative_top_padding, self.relative_bottom_padding)

        # Padding summations
        total_width_padding = sum((self.relative_left_padding, self.relative_right_padding))
        total_height_padding = sum((self.relative_top_padding, self.relative_bottom_padding))

        # Prevent cropping padding
        width_padding = total_height_padding * 1 / image_aspect_ratio[0]
        predicted_left_padding, predicted_right_padding = split_by_ratio(width_padding, width_padding_aspect_ratio)

        height_padding = total_width_padding * image_aspect_ratio[0]
        predicted_top_padding, predicted_bottom_padding = split_by_ratio(height_padding, height_padding_aspect_ratio)

        c_left, c_right, c_top, c_bottom = self.calculate_critical_padding(predicted_left_padding, predicted_right_padding, predicted_top_padding, predicted_bottom_padding)

        if c_left < 0 or c_right < 0:
            print('Switching to Axis = 1')

            self.AXIS = 1

        if c_top < 0 or c_bottom < 0:
            print('Switching to Axis = 0')

            self.AXIS = 0

        if self.AXIS == 0:
            self.calculate_y_axis_padding(
                image_aspect_ratio,
                total_height_padding,
                width_padding_aspect_ratio
            )
        else:
            self.calculate_x_axis_padding(
                image_aspect_ratio,
                total_width_padding,
                height_padding_aspect_ratio
            )

    def zoom_square(self):
        image_height = self.rgb_image.shape[0]
        image_width = self.rgb_image.shape[1]

        if image_width < image_height:
            diff = image_height - image_width
            relative_diff = diff / 2

            self.rgb_image = np.delete(self.rgb_image, range(0, round(relative_diff)), 0)
            self.rgb_image = np.delete(self.rgb_image, range(image_height - round(relative_diff), image_height), 0)

        else:
            diff = image_width - image_height
            relative_diff = diff / 2

            self.rgb_image = np.delete(self.rgb_image, range(0, round(relative_diff)), 1)
            self.rgb_image = np.delete(self.rgb_image, range(self.rgb_image.shape[1] - round(relative_diff), self.rgb_image.shape[1]), 1)

        image_height = self.rgb_image.shape[0]
        image_width = self.rgb_image.shape[1]

        if image_height > image_width:
            diff = image_height - image_width
            self.rgb_image = np.delete(self.rgb_image, range(0, diff), 0)
        elif image_height < image_width:
            diff = image_width - image_height
            self.rgb_image = np.delete(self.rgb_image, range(0, diff), 1)

    def make_square(self):
        # TODO: This is slow as hell, should time and optimise.
        image = self.rasterize_array(self.rgb_image)
        top, bottom, left, right = self.calculate_padding(image, True)

        image_height = self.rgb_image.shape[0]
        image_width = self.rgb_image.shape[1]

        if image_width < image_height:
            diff = image_height - image_width
            relative_diff = diff / 2

            if relative_diff > top or relative_diff > bottom:
                width = True
            else:
                self.rgb_image = np.delete(self.rgb_image, range(0, round(relative_diff)), 0)
                self.rgb_image = np.delete(self.rgb_image, range(image_height - round(relative_diff), image_height), 0)
                return

        else:
            diff = image_width - image_height
            relative_diff = diff / 2

            if relative_diff > left or relative_diff > right:
                width = False
            else:
                self.rgb_image = np.delete(self.rgb_image, range(0, round(relative_diff)), 1)
                self.rgb_image = np.delete(self.rgb_image, range(self.rgb_image.shape[1] - round(relative_diff), self.rgb_image.shape[1]), 1)
                return

        if width:
            diff = image_height - image_width
            relative_diff = diff / 2

            padding_color = self.rgb_image[0][0]
            self.rgb_image = insert_image_padding(self.rgb_image, padding_color, left_padding=round(relative_diff), right_padding=round(relative_diff))
        else:
            diff = image_width - image_height
            relative_diff = diff / 2

            padding_color = self.rgb_image[0][0]
            self.rgb_image = insert_image_padding(self.rgb_image, padding_color, top_padding=round(relative_diff), bottom_padding=round(relative_diff))

        image_height = self.rgb_image.shape[0]
        image_width = self.rgb_image.shape[1]

        if image_height > image_width:
            diff = image_height - image_width
            self.rgb_image = np.delete(self.rgb_image, range(0, diff), 0)
        elif image_height < image_width:
            diff = image_width - image_height
            self.rgb_image = np.delete(self.rgb_image, range(0, diff), 1)

    def calculate_critical_padding(self, left_padding, right_padding, top_padding, bottom_padding):
        left_critical = left_padding + self.left_padding
        right_critical = right_padding + self.right_padding
        top_critical = top_padding + self.top_padding
        bottom_critical = bottom_padding + self.bottom_padding

        return left_critical, right_critical, top_critical, bottom_critical

    def apply_padding(self):
        padding_color = self.rgb_image[0][0]
        self.rgb_image = calculate_image_padding(
            self.rgb_image,
            padding_color,
            round(self.relative_top_padding),
            round(self.relative_bottom_padding),
            round(self.relative_left_padding),
            round(self.relative_right_padding)
        )

    def resize_image(self, image):
        return image.resize(self.image_original_size, Image.ANTIALIAS)

    def save_image(self):
        image = Image.fromarray(self.rgb_image)

        if self.KEEP_SIZE:
            image = self.resize_image(image)

        if self.GRAY_SCALE:
            image.convert('CMYK').save(self.OUTPUT_DIRECTORY + '/' + self.FILENAME)
            # image.convert('L').save(self.OUTPUT_DIRECTORY + '/' + self.FILENAME)
        else:
            image.save(self.OUTPUT_DIRECTORY + '/' + self.FILENAME)

    def set_image_padding(self, filename=None, height=None, width=None, keep_aspect=None, keep_size=None, square=True):
        if filename:
            self.FILENAME = filename

        if keep_aspect:
            self.KEEP_ASPECT_RATIO = keep_aspect
        else:
            self.KEEP_ASPECT_RATIO = None

        if square:
            self.SQUARE = square

        if keep_size:
            self.KEEP_SIZE = keep_size
        else:
            self.KEEP_SIZE = keep_size

        if height:
            self.HEIGHT_THRESHOLD = height

        if width:
            self.WIDTH_THRESHOLD = width

        if self.BASE_DIRECTORY is None:
            raise Exception('Input directory has not been set.')

        if self.OUTPUT_DIRECTORY is None:
            raise Exception('Output directory has not been set.')

        if self.FILENAME is None:
            raise Exception('Filename has not been set')

        if self.HEIGHT_THRESHOLD is None:
            raise Exception('Height has not been set')

        if self.WIDTH_THRESHOLD is None:
            raise Exception('Width has not been set')

        print('Processing image ' + self.FILENAME)

        self.load_image()

        isolated = sample_corners(self.image_data, self.BRIGHT_COLOR_TOLERANCE)

        if not isolated:
            self.zoom_square()

        else:
            self.rasterize_image()
            self.calculate_padding(self.raster_image)
            self.calculate_relative_padding()

            if self.KEEP_ASPECT_RATIO:
                self.account_for_aspect_ratio()

            self.apply_padding()

            if self.SQUARE:
                self.make_square()

        self.save_image()
