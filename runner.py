from src.padding import Padding
import sys

if len(sys.argv) != 6:
    print('Invalid number of arguments passed.')
    exit()

FILE_NAME = sys.argv[1]
HEIGHT_THRESHOLD = int(sys.argv[2])
WIDTH_THRESHOLD = int(sys.argv[3])

if sys.argv[4] == 'yes':
    KEEP_ASPECT_RATIO = True
else:
    KEEP_ASPECT_RATIO = False

if sys.argv[5] == 'yes':
    KEEP_SIZE = True
else:
    KEEP_SIZE = False

engine = Padding(axis=1, height=HEIGHT_THRESHOLD, width=WIDTH_THRESHOLD, keep_aspect=KEEP_ASPECT_RATIO, keep_size=KEEP_SIZE)

engine.set_input_directory('storage')
engine.set_output_directory('storage/outputs')

engine.set_image_padding(filename=FILE_NAME)
